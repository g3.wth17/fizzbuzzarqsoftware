
public class FizzBuzz {
	public String evaluateNumber(int number) {
		String answer = String.valueOf(number);
		if (number % 3 == 0 && number % 5 == 0) //Extract this to a separate method such as dividing into a number
			answer = "FizzBuzz";                    
		if(number % 3 == 0 && number % 5 != 0)
			answer = "Fizz"; 
		if(number % 5 == 0 && number % 3 !=0)
			answer = "Buzz";
		return answer;
	}
	void generateAHundred() {
		for(int i = 0; i<100;i++)
		{
			System.out.println(evaluateNumber(i));
		}
	}
	
}
