import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FizzBuzz2Test {

	private final FizzBuzz2 tester = new FizzBuzz2();
	@Test
	void testEvaluateNumber1() {
		assertEquals("1", tester.evaluateNumber(1));
	}
	
	@Test
	void testEvaluateNumber3() {
		assertEquals("Fizz", tester.evaluateNumber(3));
	}
	
	@Test
	void testEvaluateNumber5() {
		assertEquals("Buzz", tester.evaluateNumber(5));
	}
	
	@Test
	void testEvaluateNumber3Multiples() {
		assertEquals("Fizz", tester.evaluateNumber(6));
	}
	
	@Test
	void testEvaluateNumber5Multiples() {
		assertEquals("Buzz", tester.evaluateNumber(10));
	}
	
	@Test
	void testEvaluateNumber3And5Multiples() {
		assertEquals("FizzBuzz", tester.evaluateNumber(15));
		assertEquals("FizzBuzz", tester.evaluateNumber(45));
	}


}
