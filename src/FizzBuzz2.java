
public class FizzBuzz2 {
	
	private static final int _BUZZ = 5;
	private static final int _FIZZ = 3;

	public String evaluateNumber(int numero) {
		if (esMultiploDe(numero, _FIZZ) && esMultiploDe(numero,_BUZZ))
			return "FizzBuzz";                    
		if(esMultiploDe(numero, _FIZZ))
			return "Fizz";
		if(esMultiploDe(numero,_BUZZ))
			return "Buzz";
		return String.valueOf(numero);
	}

	private boolean esMultiploDe(int numero, int multiplo) {
		return numero % multiplo == 0;
	}	
}
